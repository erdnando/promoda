import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    dbCargada:'',
    usuario:"gvargas",
    contrasenia:"gvargas01",
    autenticado:false,
    database:[],
    archivo:'Nombre',
    archivoSize:'Tamaño',
    archivoTipo:'Tipo',
    archivoUltimaFecha:'Última fecha modificación',
    archivoRegistros:0,
  },
  mutations: {

  },
  actions: {

  }
})
